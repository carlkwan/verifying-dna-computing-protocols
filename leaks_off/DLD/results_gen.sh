#!/bin/bash
for i in 1 5 10 15 20 
do
	echo "---- N=$i start ----"
	./prism dld_leaks_off_N_"$i".sm correctness.csl -exportresults results_correctness_dld_leaks_off_N_"$i".txt
	./prism dld_leaks_off_N_"$i".sm performance.csl -const t=0:1000:70000 -exportresults results_performance_dld_leaks_off_N_"$i".txt
	./prism dld_leaks_off_N_"$i".sm performance_csv.csl -const t=0:1000:70000 -exportresults results_performance_dld_leaks_off_N_"$i".csv:csv
	echo "---- N=$i finish ----"
done
