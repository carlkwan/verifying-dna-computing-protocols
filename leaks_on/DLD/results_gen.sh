#!/bin/bash
for i in 1 5 10 15 20 25 30
do
	echo "---- N=$i start ----"
	./prism DLD-transducer-N-"$i".sm correctness.csl -exportresults results_correctness_N_"$i".txt
	./prism DLD-transducer-N-"$i".sm performance.csl -const t=0:1000:70000 -exportresults results_performance_N_"$i".txt
	./prism DLD-transducer-N-"$i".sm performance_csv.csl -const t=0:1000:70000 -exportresults results_performance_N_"$i".csv:csv
	echo "---- N=$i finish ----"
done
