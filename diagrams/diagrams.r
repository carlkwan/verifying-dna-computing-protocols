#!/usr/bin/env Rscript
reso = 260

colors = c("red", "green", "purple", "cyan");
num = c(1, 5, 10, 15, 20);
strs = c("dld", "sld", "tld");
w = 10
h = 7

# LEAKS OFF
cat(":::::::::::::::::::::::::::::LEAKS OFF::::::::::::::::::::::::::::::::\n");

prefix = "results_performance_";
suffix = "_leaks_off.csv";

for (str in strs) {
	cat("========================================================\n");
	cat("PLOTTING ");
	cat(toupper(str));
	cat("\n");

	# export plot to .png file
	fname = paste(str, sep="", "_leaks_off.png");
	cat("Printing to file ");
	cat(fname);
	cat("\n");
	png(filename = fname, width=w, height=h, units="in", res=reso);

	# get data from file
	fname = paste(paste(prefix, sep="", str), sep="", suffix);
	data = read.csv(fname);

	# plotting data
	x = data[[1]][0:40];
	y = data[[2]][0:40];

	plot (x, y, type = "l", 
		    col="blue",
		    main= paste(toupper(str), "Performance (Leaks Off)"),
	  	    xlab = "Seconds",
		    ylab = "Probability of Completion")
		    # line = paste("N =", as.character(num[1])));
	for (i in c(3,4,5,6)) {
		y = data[[i]][0:40];
		y 
		lines(x, y, type="l", 
			    col=colors[i-2])
			    #line = paste("N =", as.character(num[i]-1)));
	}
	cat("PLOTTING ")
	cat(toupper(str))
	cat(" FINISHED\n")
	cat("========================================================\n");
}

# LEAKS ON

cat(":::::::::::::::::::::::::::::LEAKS ON::::::::::::::::::::::::::::::::\n");

affix = "_leaks_on_results_performance_N_"; 

for (str in strs) {
	cat("========================================================\n");
	cat("PLOTTING ");
	cat(toupper(str));
	cat("\n");

	# export plot to .png file
	fname = paste(str, sep="", "_leaks_on.png");
	cat("Printing to file ");
	cat(fname);
	cat("\n");
	png(filename = fname, width =w, height=h,units="in", res=reso);

	# get data from file
	fname = paste(paste(str, sep="", affix), sep="", "1.csv");
	data = read.csv(fname);

	# plotting data
	x = data[[1]][0:40];
	y = data[[2]][0:40];

	plot (x, y, type = "l", 
		    col="blue",
		    main= paste(toupper(str), "Performance (Leaks On)"),
	  	    xlab = "Seconds",
		    ylab = "Probability of Completion")
		    # line = paste("N =", as.character(num[1])));
	for (i in c(3,4,5,6)) {
		suffix = paste(as.character(num[i-1]), sep="", ".csv");
		fname = paste(paste(str, sep="", affix), sep="", suffix);
		data = read.csv(fname);
		y = data[[2]][0:40];
		lines(x, y, type="l", 
			    col=colors[i-2])
			    #line = paste("N =", as.character(num[i]-1)));
	}
	cat("PLOTTING ")
	cat(toupper(str))
	cat(" FINISHED\n")
	cat("========================================================\n");
}


# DELTA CALCULATIONS
cat(":::::::::::::::::::::::::::::DELTA PLOTS::::::::::::::::::::::::::::::\n");
suffix = "_E_t_delta.csv"
colors = c("blue", "red", "green");

for (str in strs) {
	cat("========================================================\n");
	cat("PLOTTING ")
	cat(toupper(str));
	cat(" DELTA\n")
	fname = paste(str, sep="", suffix);
	data = read.csv(fname);
	N = data[[1]];
	delta = data[[2]] - data[[3]];

	cat(delta)
	cat("\n")

	png(filename = paste(str,sep="", "_delta.png"), width=w,height=h,units="in", res = reso);
	plot(N, delta, type = "b");
	cat("========================================================\n");
	cat("PLOTTING")
	cat(toupper(str));
	cat(" DELTA FINISHED\n")
}


png(filename = "all_delta.png",width=w,height=h,units="in", res=reso);
plot(N, delta, type = "n", xlab=);
i = 1;
for (str in strs) {
	
	fname = paste(str, sep="", suffix);
	data = read.csv(fname);
	N = data[[1]];
	delta = data[[2]] - data[[3]];
	lines(N, delta, type = "b", col = colors[i]);
	i = i + 1;
}
