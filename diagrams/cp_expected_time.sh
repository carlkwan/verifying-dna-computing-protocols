#!/bin/bash

for leak in "leaks_on" "leaks_off"
do
	echo $leak;
	for dir in $(cd $leak; ls)
	do
		for fname in $(cd $leak/$dir; ls *performance*.txt)
		do
			echo $fname;
			cp $leak/$dir/$fname ./${dir}_$fname ;
		done
	done
done
